#!/bin/bash

Help() {
    echo "-h    help"
    echo "-b    build the project"
    echo "-r    run the project"
}

while getopts ":bhr" arg; do
    
    case $arg in
        h) 
            Help
            exit;;
        b)
            go build -o bin/main main.go
            exit;;
		r) 
			go run .
			exit;;
        \?)
            echo "Invalid option: pass -h for help"
            exit;;
    esac
done

if [ $OPTIND -eq 1 ]
then 
    echo "No arguments passed: pass -h for help"
fi

