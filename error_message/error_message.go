package error_message

import "log"

func ErrorMessage(err error, message string) {
	/* invoke error message there is an error */
	if err != nil {
		log.Fatal(message, err)
	}
}
