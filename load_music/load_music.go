package load_music

import (
	"github.com/veandco/go-sdl2/mix"
	"gotk3-experiment/error_message"
)

func LoadMusic(music_path string) (mus *mix.Music) {
	music, err := mix.LoadMUS(music_path)

	error_message.ErrorMessage(err, "Could not open audio file")

	return music
}
