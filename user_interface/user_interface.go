package user_interface

import (
	"github.com/gotk3/gotk3/gtk"
	"gotk3-experiment/error_message"
)

func UserInterface(application *gtk.Application) {
	/* Design the User Interface of the Application */

	// create box
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 12)

	// invoke error message there is an error
	error_message.ErrorMessage(err, "Unable to create box")

	// create button 1
	b1, err := gtk.ButtonNewWithLabel("Button 1")

	error_message.ErrorMessage(err, "Unable to create button")

	// create button 2
	b2, err := gtk.ButtonNewWithLabel("Button 2")

	error_message.ErrorMessage(err, "Unable to create button")

	// Add buttons to box
	box.Add(b1)
	box.Add(b2)

	// Create a new toplevel window, set its title, and connect it to the
    // "destroy" signal to exit the GTK main loop when it is destroyed.
    appWindow, err := gtk.ApplicationWindowNew(application)

	error_message.ErrorMessage(err, "Unable to create a window")

	// Set window title 
    appWindow.SetTitle("Music Player")

	// Close button of the window
    appWindow.Connect("destroy", func() {
        gtk.MainQuit()
    })

	// Add box to window
	appWindow.Add(box)

    // Set the default window size.
    appWindow.SetDefaultSize(800, 600)

    // Recursively show all widgets contained in this window.
    appWindow.ShowAll()

}
