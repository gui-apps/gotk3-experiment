package main

import (
	"os"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/mix"

	"gotk3-experiment/error_message"
	"gotk3-experiment/user_interface"
	"gotk3-experiment/load_music"

	)

func main() {
    // Initialize GTK without parsing any command line arguments.
	sdl.Init(sdl.INIT_AUDIO)

	mix.OpenAudio(48000, mix.DEFAULT_FORMAT, 2, mix.DEFAULT_FREQUENCY)

	mix.VolumeMusic(12)

	music := load_music.LoadMusic("test_audio_files/calm.ogg")

	music.Play(-1)

    const appID = "org.gtk.exmaple"

	application, err := gtk.ApplicationNew(appID, glib.APPLICATION_FLAGS_NONE)

	error_message.ErrorMessage(err, "Could not create application")

	application.Connect("activate", func() { user_interface.UserInterface(application) })

	os.Exit(application.Run(os.Args))
}

