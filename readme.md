# gotk3-experiment

Lets go and have some fun learning about the go programming language while using the gotk3 library for building gtk3 applications.

## Building the Project

Make the script executable
```
chmod +x build.sh
```

Pass -b flag to get help with this script
```
./build -h
```

Output ==>
```
-h    help
-b    build the project
-r    run the project
```

To build the project, pass the -b flag
```
./build.sh -b
```

To run the project, pass the -r flag
```
./build.sh -r
```

